## Introdução
Ainda hoje, mesmo com diversos recursos, os desenvolvedores passam pelo desafio de criar uma ilusão de uma conexão persistente na Web. Isto ocorre por conta do funcionamento do protocolo HTTP, que em cada requisição de uma página a conexão é fechada logo após o envio. Para "burlar" este comportamento do protocolo HTTP, os desenvolvedores criaram uma técnica chamada AJAX que cria esta ilusão de conexão persistente através de requisições GET/POST que ocorrem no *background* em um intervalo de tempo setado pelo desenvolvedor. Essas conexões vão para WebServices que retornam mensagens em XML ou JSON e são usadas por exemplo, nos clientes web de email; o desenvolvedor configura um determinado intervalo de tempo para fazer requisições em um WebService que retorna um objeto JSON ou XML com os últimos emails recebidos e não visualizados. E usando DOM, ele torna a página dinâmica, neste caso quando um novo email é recebido ele logo é adicionado a lista de emails sem necessidade de dar um *refresh* na página. Para muitas aplicações esta técnica funciona muito bem, no entanto para aplicações que exigem um intervalo de tempo menor, quase simultanêo como jogos ou clientes de chat esta técnica se mostra problemática, e é neste cenário que surgiu o WebSocket, uma especificação de um protocolo que permite uma conexão persistente entre o cliente e o servidor, especialmente projetado para rodar em cima de navegadores e servidores web.

## Arquitura WebSocket
![Arquitetura WebSocket](http://arquivo.devmedia.com.br/REVISTAS/front_end/imagens/edicao8/1/1.png)

Como podemos ver na imagem acima, o cliente faz uma requisição para o servidor WebSocket que envia uma outra requisição para a aplicação. Ou seja, o WebSocket contém a implementação para se comunicar com o protocolo desta aplicação, seja ela um jogo, um cliente XAMPP ou no caso deste trabalho, um cliente IRC. Após isso, ele reenvia essa requisição para web que depois voltará ao cliente. Fazendo uma analogia, o WebSocket funciona como um proxy, intermediando a conexão do cliente com o servidor de uma determinada aplicação. Graças a isto ele é bastante eficaz contra *firewalls*. Abaixo, temos uma imagem da arquitetura de um proxy para entermos esta analogia melhor:

![Arquitetura Proxy](https://www.ibm.com/developerworks/br/tivoli/library/0909_haverlock/images/figure1.gif)

Abaixo podemos ver uma representação visual do nosso projeto:

![Arquitetura Projeto](https://cdn.pbrd.co/images/1eCDjbSoQ.png)

## Estabelecendo uma conexão com um WebSocket.
Como uma expêriencia didatica, vamos ver como funciona uma requisição WebSocket. A requisição é bastante similar a que faríamos com uma conexão web comum, apenas adicionando um cabeçalho de atualização o que indicara ao servidor que o cliente pretende estabelecer uma conexão WebSocket:

> GET ws://exemplo.websocket.com.br/ HTTP/1.1

> Origin: http://exemplo.com

> Connection: Upgrade

> Host: exemplo.websocket.com

> Upgrade: websockets

É importante salientar que na url de um WebSocket, nós usamos o *schema* ws, em uma analogia com a web é equivalente ao http de uma url comum, também temos o wss que é equivalente ao https. Pois é adicionado uma camada de criptografia SSL. Após esta requisição inicial, o servidor deverá nos responder:

> HTTP 101 WebSocket Protocol Handshake

> Date: Sun, 30 Apr 2017 11:17:34 GMT

> Connection: Upgrade

> Upgrade: WebSocket

Pronto!Nossa conexão foi concluída e o HTTP inicial foi substituido por uma conexão WebSocket. Agora estamos aptos para trocar mensagens com o servidor. Uma mensagem em um WebSocket é constituida por um ou mais *frames*, sendo que o tamanho de cada um desses *frames* variam de 4 a 12 bytes, isso é feito para diminuir a quantidade de dados enviados em um pacote.

### Com JS(Frontend)
Vimos como estabelecer uma conexão com WebSocket de forma teórica, agora veremos na prática usando JavaScript(JS). Estabelecer uma conexão com WebSockets com JS é bastante simples, basta instanciar o objeto WebSocket, passando no seu método construtor a URL do WebSocket:

> var socket = new WebSocket('ws://echo.websocket.org');

Já com a conexão estabelecida, podemos criar os métodos *callbacks* para os eventos WebSocket. Por exemplo: podemos criar um *callback* para ser chamado quando o socket for estabelecido, no evento onopen:

> //apresenta a mensagem "Conectado com: [url]"

> socket.onopen = function(event) {

>    console.log('Conectado com: ' + event.currentTarget.URL);

>  };

Para recebermos as mensagens enviadas pelo servidor, nós criamos o método onmessage e chamamos o método data do objeto event:

> //apresenta uma mensagem enviada pelo servidor

>  socket.onmessage = function(event) {

>    var mensagem = event.data;

>    console.log(mensagem);

> };

E para enviarmos uma mensagem para o servidor, basta chamar o método send e passar a mensagem como parâmetro:

> socket.send("mensagem");

Finalmente, para fechar a conexão basta chamar o método close:

> socket.close();

## Criando um servidor WebSocket em NodeJS(Backend)

Para criar um servidor WebSocket no NodeJS é necessário usar uma API chamada socket.io, esta API não está disponível por padrão e deverá ser instalada com o seguindo comando:

> npm install socketio

Agora, deverá ser importada as bibliotecas e habilitar a porta 8080 para conexão:

> var http = require('http'), io = require('socket.io');

> var server = http.createServer(function(req, res){ 

> //envia cabeçalho e mensagem 

>	res.writeHead(200,{ 'Content-Type': 'text/html' }); 

>	res.end('<h1>Ola mundo!</h1>');

> });

> server.listen(8080);

> //Cria uma instância socket.io passando como parâmetro uma instância do servidor

> var socket = io.listen(server);

Com nosso servidor habilitado para escutar na porta 8080, podemos adicionar os *callbacks* para os eventos. Começaremos, adicionando um *listener* para a conexão:

> socket.on('connection', function(client){  

Dentro desse *listener*, podemos colocar os *callbacks* para receber as mensagens:

> socket.on('connection', function(client){ 
>	client.on('message',function(event){ 
>		console.log(event);
>	});

E usamos o método broadcast.emit para enviar uma mensagem para todos os clientes:

> socket.broadcast.emit('message');

Você pode especificar o cliente pela sua *session id* caso queira enviar para um cliente especifico:

> socket.broadcast.to(socketid).emit('message');



