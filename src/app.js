var fs = require('fs'),
socketio = require('socket.io'),
bodyParser = require('body-parser'),  // processa corpo de requests
cookieParser = require('cookie-parser'),  // processa cookies
irc = require('irc'),
app = require('express')(),
express = require('express'),
server = require('http').Server(app),
path = require('path');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
server.listen(3000);
console.log("Rodando na porta 3000");
var canal = "";
var servidor = "";
var nick = "";
var usuarios = [];

app.use(express.static(path.join(__dirname, 'public')));

app.post('/login', function (req, res) { 
   nick = req.body.nome;
   canal = req.body.canal;
   servidor = req.body.servidor;
   res.redirect('/index');
});


app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/login.html'));
});

app.get('/index', function (req, res) {
  if(nick!="" && canal != "" && servidor != ""){
    res.cookie('nick', nick);
    res.cookie('canal', canal);
    res.cookie('servidor', servidor);
    res.sendFile(path.join(__dirname, '/index.html'));
  }
});

io = socketio.listen(server);

io.on('connection', function (socket) {
      console.log('conectado');
	  var irc_client = new irc.Client(
				 servidor, 
				 nick,
				{channels: [canal],});
      
	  irc_client.addListener('message'+canal, function (from, message) {
		console.log(from + ' => '+ canal +': ' + message);
		socket.emit('message', from + ' => '+ canal +': ' + message);
	  });
	  // disparado quando o cliente estiver conectado
	  irc_client.addListener('registered', function(message) {
	    console.log('conectado:'+JSON.stringify(message));
        irc_client.join(canal, function(){
          socket.emit('registered', 'Conectado em: irc://'+nick+'+@'+servidor+'/'+canal);
          //irc_client.send('NAMES', canal);
        });
	    //socket.emit('join', 'Conectado em: irc://'+nick+'+@'+servidor+'/'+canal);
        //usuarios.push({"servidor:" + server, "nick:" + nick, "canal:" + channel});
	  });
      irc_client.addListener('error', function(msg) {
        console.log(msg);
        socket.emit('erro', msg);  
      });
      /*irc_client.addListener('message', function(nick, to, text, message){
        console.log(nick + ' -> '  + message)
      });*/  
  
	  socket.on('message', function (msg) {
        msg = JSON.parse(msg);
		console.log('Mensagem recebida: ', msg.msg);
		irc_client.say(msg.canal, msg.msg );
	  });

      socket.on('mode', function (mode) {
        irc_client.send('mode', canal, mode);
      });
   
      irc_client.addListener('+mode', function(channel,by,mode,argument,message) {
        socket.emit('message', '<font color = "Green">' +
           by + ' alterou o mode de ' + argument +' para: ' + mode + '</font>');
      });
     

      irc_client.addListener('names', function(channel, nicks){
         console.log(JSON.stringify(nicks));
         socket.emit('names', JSON.stringify(nicks));
      });
 
     irc_client.addListener('raw', function(message) {
       console.log('mensagem recebida:'+JSON.stringify(message));
       if(message.command == "PRIVMSG"){
			socket.emit('message', message.nick +
               ' => '+ message.args[0] +': ' + message.args[1]);
       }
       if(message.command == "PART"){
           socket.emit('part', message.nick);
       }      
       if(message.command == "JOIN"){
          socket.emit('join', message.nick); 
       }
     });

	  socket.on('command', function (msg) {
		console.log('comando recebido: ', msg);
		irc_client.send(msg);
	  });
});
