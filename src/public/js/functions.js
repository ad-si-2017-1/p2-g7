/*
  Adiciona <mensagem> no <elemento_id>. 
*/
function adiciona_mensagem(mensagem,elemento_id,timestamp) {
	var novo_elemento = document.createElement('div');
	novo_elemento.id = "mensagem"+timestamp;
	document.getElementById(elemento_id).appendChild(novo_elemento);
	document.getElementById('mensagem'+timestamp).innerHTML=mensagem;
}

/*
  Transforma timestamp em formato HH:MM:SS
*/

function addUsuarioSelecao(usuarios){
  for (var key in usuarios){
    var novo_elemento = document.createElement('option');
    novo_elemento.text = key;
    document.getElementById('usuarios').add(novo_elemento); 
  }
}

function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
function carrega_mensagens(elemento_id, timestamp) {
	var mensagem = "";
	var horario = "";
	$.get("obter_mensagem/"+timestamp, function(data,status) {
		if ( status == "success" ) {
		    var linhas = data;
		    for ( var i = linhas.length-1; i >= 0; i-- ) {
		    	horario = timestamp_to_date(linhas[i].timestamp);
			mensagem = 
				"["+horario+" - "+
				linhas[i].nick+"]: "+
		                linhas[i].msg;
			novo_timestamp = linhas[i].timestamp;
		    	adiciona_mensagem(mensagem,elemento_id,novo_timestamp);
			}
		}
		else {
		    alert("erro: "+status);
		}
		}
	);
	t = setTimeout( 
		function() { 
			carrega_mensagens(elemento_id,novo_timestamp) 
		}, 
		1000);		
}

function remove_usuario(usuario){
  var select = document.getElementById('usuarios');
  for(var i = 0; i<select.length; i++){
    if (select.options[i].value == usuario){
      select.remove(i);
    }
  }
}

var socket;
function trocarMode(elemento){
    var mode = document.getElementById(elemento).value;
	socket.emit('mode', mode);
}


function conectarNoWebSocket(){ 
   socket = io.connect('/');
   socket.join = false;
   $("#status").text("Tentando conectar em - irc://"+
     Cookies.get("nick")+"@"+
     Cookies.get("servidor")+"/"+
     Cookies.get("canal") + '...');
     socket.on('connect', function(){
        socket.join = false;
        nick = Cookies.get("nick");
        canal = Cookies.get("canal");
        servidor = Cookies.get("servidor");
        socket.on('registered', function(data) {
            socket.join = true;
            $("#status").text(data);
            var novo_elemento = document.createElement('option');
            novo_elemento.text = Cookies.get("canal");
            document.getElementById('usuarios').add(novo_elemento);
        });
        socket.on('names', function(data){
            nomes = JSON.parse(data);
            addUsuarioSelecao(nomes);
            //adiciona_mensagem(data, 'mural', new Date());
        });
        socket.on('message', function(data){
            adiciona_mensagem(data, 'mural', new Date());
        });
        socket.on('erro', function(data){
           adiciona_mensagem('<font color = "red">' + data.server + '  => ' + data.command +  '</font>', 'mural', new Date());
        });
        socket.on('part', function(data){
           adiciona_mensagem('<font color = "red">' + data  + ' saiu da sala.</font>', 'mural', new Date());
           remove_usuario(data);
        });
        socket.on('join', function(data){
           add_usuario(data);
        });
       });
}

function add_usuario(usuario){
  adiciona_mensagem('<font color = "green">' + usuario  + ' entrou na sala.</font>', 'mural', new Date());
  var repetido = false;
  var select = document.getElementById('usuarios');
  for(var i = 0; i<select.length; i++){
    if (select.options[i].value == usuario){
      repetido = true;
    }
  }
  if(!repetido){
    var novo_elemento = document.createElement('option');
    novo_elemento.text = usuario;
    document.getElementById('usuarios').add(novo_elemento); 
  }
}

function enviaMensagem(){
  if(socket.join){
    var mensagem = {};
    mensagem.msg = document.getElementById('mensagem').value;
    mensagem.canal = document.getElementById('usuarios').value;
    console.log(canal); 
    if (mensagem.msg.lastIndexOf("/", 0) === 0){
      socket.emit('command', mensagem.msg.split("/")[1]);
    }
    else{
      var data = Cookies.get('nick') + ' => ' + mensagem.canal+': ' + mensagem.msg;
      socket.emit('message', JSON.stringify(mensagem));
      adiciona_mensagem(data, 'mural', new Date());
    }
  }
  document.getElementById('mensagem').value = "";
}
  


